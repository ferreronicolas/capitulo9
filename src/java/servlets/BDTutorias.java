
package servlets;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class BDTutorias {
    
    private Connection conexion;
    private Statement sentenciaSQL;
    private ResultSet cdr;

    public BDTutorias() throws ClassNotFoundException, SQLException,
        InstantiationException, IllegalAccessException{
        
        // Cargar el controlador JDBC
        String controlador = "com.mysql.jdbc.Driver";
        Class.forName(controlador).newInstance();
        conectar(); // Conectar con la fuente de datos
    
    }
    
    public void conectar() throws SQLException {
        String URL_bd = "jdbc:mysql://localhost:3306/bd_tutorias";
        String usuario = "root";
        String contraseña = "123456";
        
        // Conectar con la BD
        conexion = DriverManager.getConnection(URL_bd,usuario,contraseña);
        // Crear una sentencia SQL
        sentenciaSQL = conexion.createStatement(
            ResultSet.TYPE_SCROLL_INSENSITIVE,
            ResultSet.CONCUR_UPDATABLE);
        System.out.println("\nConexion realizada con exito.\n");
    }
    
    public void cerrarConexion() throws java.sql.SQLException{
        if(cdr != null) cdr.close();
        if(sentenciaSQL != null) sentenciaSQL.close();
        if(conexion != null) conexion.close();
    }
    
    public ResultSet obtenerTabla(String tabla) throws SQLException{
        cdr = sentenciaSQL.executeQuery("SELECT * FROM " + tabla);
        return cdr;        
    }
    
    public ResultSet tutoriasDisponibles() throws SQLException{
        cdr = sentenciaSQL.executeQuery(""
                + "SELECT tutorias.id_tutoria, profesores.profesor,"
                + "tutorias.dia, tutorias.hora "
                + "FROM profesores, tutorias "
                + "WHERE profesores.id_profesor=tutorias.id_profesor "
                + "AND tutorias.id_tutoria NOT IN "
                + "(SELECT id_tutoria FROM citas)");
        return cdr;
    }
    
    public void insertarEnCitas(int id_tutoria, String alumno, String asunto)  throws  SQLException{
        
        sentenciaSQL.executeUpdate("INSERT INTO citas" + 
                " VALUES (" + id_tutoria + ", '" + alumno + "', '" + asunto + "')"
        );
    }
    
    public void borrarCitas() throws SQLException{
        sentenciaSQL.executeUpdate("DELETE FROM citas");
    }
    
    public int obtenerIdProfesor(String nombre) throws SQLException{
        cdr = sentenciaSQL.executeQuery(
            "SELECT * FROM profesores" + 
            " WHERE profesor = " + "'" + nombre + "'");
        if(cdr.isBeforeFirst()){
            cdr.first();
            return cdr.getInt("id_profesor");
        }else
            return -1;
    }
    
    public int obtenerIdTutoria(int id_prof, String d, String h) throws SQLException{
        
        cdr = sentenciaSQL.executeQuery("SELECT * "
                + "FROM tutorias"
                + " WHERE id_profesor = " + id_prof
                + " AND dia = " + "'" + d + "'"
                + " AND hora = " + "'" + h + "'");
        if(cdr.isBeforeFirst()){
            cdr.first();
            return cdr.getInt("id_tutoria");
        }else
            return -1;
    }
    
    public boolean noEstaReservada(int idT) throws SQLException{
        cdr = sentenciaSQL.executeQuery("SELECT "
                + "id_tutoria "
                + "FROM citas "
                + "WHERE id_tutoria = " + idT);
        if(cdr.isBeforeFirst())
            return false; // Esta reservada
        else
            return true; // no esta reservada
    }
        
}
