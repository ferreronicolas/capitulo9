
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class Consultar extends HttpServlet {

    Connection conexion = null;
    Statement sentenciaSQL = null;
    ResultSet cdr = null;
    
    @Override
    public void init(ServletConfig config) throws ServletException {
        
        super.init(config);
        try{
            // Cargar y registrar el controlador JDBC
            String controlador = "com.mysql.jdbc.Driver";
            Class.forName(controlador).newInstance();
            
            // Conectar con la fuente de datos
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/bd_tutorias","root","123456");
            
            // Crear una sentencia SQL
            sentenciaSQL = conexion.createStatement();
        }catch(ClassNotFoundException e){
            System.out.println("No se pudo cargar el controlador: " + 
                    e.getMessage());
        }catch(SQLException e){
            System.out.println("Excepcion SQL: " + e.getMessage());
        }catch(InstantiationException e){
            System.out.println("Objeto no creado: " + e.getMessage());
        }catch(IllegalAccessException e){
            System.out.println("Acceso Ilegal: " + e.getMessage());
        }
    }
    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        
        try{
            // Realizar una consulta SQL
            cdr = sentenciaSQL.executeQuery(
                "SELECT profesor, dia, hora, alumno " + 
                "FROM profesores, tutorias, citas " +
                "WHERE profesores.id_profesor = tutorias.id_profesor" +
                " AND tutorias.id_tutoria = citas.id_tutoria");
            
            // Mostrar el conjunto de resultados obtenidos
            out.println("<html><head><title>Tutorias</title></head>");
            out.println("<body>");
            // Tabla
            out.println("<table width=100% border=1");
            // Cabeceras
            out.println("<tr>");
            out.println("<th>Alumno</th><thDia/hora</th><Con el profesor</th>");
            out.println("</tr>");
            // Filas
            while(cdr.next()){
                out.println("<tr>");
                out.println("<td width=40%>" + cdr.getString("alumno") + "</td>");
                out.println("<td width=20%>" + cdr.getString("dia") + " a las " + cdr.getString("hora") + "</td>");
                out.println("<td width=40%>" + cdr.getString("profesor") + "</td>");
                out.println("</tr>");
            }
            out.println("</table>");
            out.println("</body></html>");
        }catch(SQLException e){
            out.println("Excepcion SQL: " + e.getMessage());
        }
    }

    @Override
    public void destroy() {
        
        try{
            if(cdr != null) cdr.close();
            if(sentenciaSQL != null) sentenciaSQL.close();
            if(conexion != null) conexion.close();
        }catch(SQLException ignorada){
            
        }
    }
    
    


}
