
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class CuentaCk extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        
        // Obtener el valor actual de la cookie "cuenta.ck" buscando
        // entre las cookies recibidas
        String scuenta = null;
        Cookie[] cookies = request.getCookies();
        if(cookies != null){
            for(int i = 0; i < cookies.length; i++){
                if(cookies[i].getName().equals("cuenta.ck")){
                    scuenta = cookies[i].getValue();
                    break;
                }
            }
        }
        /*
        Incrementar el contador para esta pagina. El valor es guardado en la cookie con el nombre
        "cuenta.ck". Despues, asugurarse de enviarsela al cliente con la respuesta (respose).
        */
        Integer objCuenta = null; // Contador
        if(scuenta == null) // SI no se encontro cuenta.ck
            objCuenta = new Integer(1);
        else    // Si se encontro cuenta.ck
            objCuenta = new Integer(Integer.parseInt(scuenta) + 1);
        // Crear una nueva cookie con la cuenta actualizada
        Cookie c = new Cookie("cuenta.ck", objCuenta.toString());
        System.out.println(c.getPath());
        // Añadir la cookie a las cabeceras de la respuesta HTTP
        response.addCookie(c);
        
        // Responder al cliente
        out.println("<html>");
        out.println("Has visitado esta pagina " + objCuenta.toString() + 
                ((objCuenta.intValue() == 1) ? " vez." : " veces."));
        out.println("</html>");
        
        // Cerrar el flujo
        out.close();
    }

    @Override
    public String getServletInfo() {
        return "Servlet CuentaCk";
    }

}
