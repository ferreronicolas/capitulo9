
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class FechaHora extends HttpServlet {


    protected void procesarPeticion(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        //response.setContentType("text/html");
        Calendar fechaHora = new GregorianCalendar();
        PrintWriter out = response.getWriter();
        
        
        
        out.println("Fecha: " + 
                fechaHora.get(Calendar.DAY_OF_MONTH) + "/" +
                (fechaHora.get(Calendar.MONTH) + 1) + "/" +
                fechaHora.get(Calendar.YEAR) );
        
        out.println("Hora: " + 
                fechaHora.get(Calendar.HOUR_OF_DAY) + ":" + 
                fechaHora.get(Calendar.MINUTE) + ":" + 
                fechaHora.get(Calendar.SECOND) );
        
        out.close();

    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        procesarPeticion(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        procesarPeticion(request, response);
    }


    @Override
    public String getServletInfo() {
        return "Servlet FechaHora";
    }

}
