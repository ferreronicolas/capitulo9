package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class HolaMundo extends HttpServlet{
    
    // Manipular las posibles peticiones enviadas por el cliente:
    // Utilizando el atributo method=get o method=post
    
    protected void procesarPeticion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        response.setContentType("text/html");
        
        PrintWriter out = response.getWriter();
        
        out.println("<html>");
        out.println("<head>");
        out.println("<title> Servlet Hola Mundo! </title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<font size=7>");
        out.println("¡¡¡Hola Mundo!!!");
        out.println("</font>");
        out.println("</body>");
        out.println("</html>");
        
        out.close();
    }
    
    // Manipular la peticion enviada por el cliente
    // utilizando el atributo method=get

    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        procesarPeticion(request, response);
    }
    
    // Manipular la peticion enviada por el cliente
    // utilizando el atributo method=post

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        procesarPeticion(req, resp);
    }
    
    // Devuelve una descripcion breve
    
    public String getServerInfo(){
        return "servlet holamundo";
    }
    
}
