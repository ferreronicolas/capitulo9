
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class SVTutorias extends HttpServlet {

    private BDTutorias BD;

    @Override
    public void init(ServletConfig config) throws ServletException {
        
        super.init(config);
        try{
            BD = new BDTutorias();
        }catch(ClassNotFoundException e){
            System.out.println("Clase no encontrada. " + e.getMessage());
        }catch(InstantiationException e){
            System.out.println("Objeto no creado. " + e.getMessage());
        }catch(IllegalAccessException e){
            System.out.println("Acceso ilegal. " + e.getMessage());
        }catch(SQLException e){
            System.out.println("Excepion SQL. " + e.getMessage());
        }
        
    }
    
    // Manipular la peticion enviada por el cliente
    // Utilizando el atributo method=post
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        // Tipo de la respuesta que sera enviada al cliente
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        
        // Tomar datos recibidos del cliente y enviarlos a la base de datos
        try{
            String profesor = request.getParameter("profesor");
            String dia = request.getParameter("dia");
            String hora = request.getParameter("hora");
            String alumno = request.getParameter("alumno");
            String asunto = request.getParameter("asunto");
            int id_profesor = -1, id_tutoria = -1;
            id_profesor = BD.obtenerIdProfesor(profesor);
            
            if (id_profesor != -1) {
                id_tutoria = BD.obtenerIdTutoria(id_profesor, dia, hora);
                if(id_tutoria != -1 && BD.noEstaReservada(id_tutoria)){
                    // Insertar una fila en la tabla citas
                    BD.insertarEnCitas(id_tutoria, alumno, asunto);
                    
                    // Responder al cliente
                    out.println("<html>");
                    out.println("<title>Respuesta a la solicitud</title>");
                    out.println("Su peticion ha sido registrada<br>Un saludo.");
                    out.println("</html>");
                    return;
                }
            }
            out.println("No se pudo atender su solicitud. Elija otra " +
                    "opcion. Si la hay, entre las siguientes:<br><br>");
            mostrarTutoriasDisponibles(out);
        }catch(SQLException e){
            out.println("Exepcion SQL. " + e.getMessage());
        }
        
        // Cerrar el flujo
        out.close();
    }
    
    // Devuelve un formulario html con las tutorias disponibles.
    public void mostrarTutoriasDisponibles(PrintWriter out){
        
        try{
            ResultSet cdr = BD.tutoriasDisponibles();
            // Mostrar el conjunto de resultados obtenidos
            out.println("<html><head><title>Tutorias</title></head>"
                    + "</body>");
            // Tabla
            out.println("<tr>"
                    + "<th>Dia/hora</th><th>Con el profesor</th>"
                    + "</tr>");
            // Filas
            while(cdr.next()){
                out.println("<tr>"
                        + "<td width=30%" + cdr.getString("dia") +
                        " a las " + cdr.getString("hora") + "</td>" +
                        "<td width=50%>" + cdr.getString("profesor") + "</td>"
                                + "</tr>");
            }
            
            out.println("</table></body></html>");
        }catch(SQLException e){
            out.println("Excepcio SQL. " + e.getMessage());
        }
    }

    @Override
    public void destroy() {
        
        try{
            BD.cerrarConexion();
        }catch(SQLException ignorada){
            
        }
    }
    

    @Override
    public String getServletInfo() {
        return "Servlet SvTutorias";
    }
}
