package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class Saludo extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Servlet Saludo</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<center>");
        for(int tamaño = 1; tamaño <= 7; tamaño++){
            out.println("<font size=" + tamaño + "color=blue>");
            out.println("¡¡¡Hola Mundo!!!");
            out.println("<br>");
        }
        out.println("<br>");
        Calendar fechahora = new GregorianCalendar();
        Random rnd = new Random();
        int tamaño = rnd.nextInt(7)+1;
        out.println("<font size=" + tamaño + ">");
        out.println("<font color=green>");
        out.println("Son las " +
                fechahora.get(Calendar.HOUR_OF_DAY) + ":" + 
                fechahora.get(Calendar.MINUTE) + ":" + 
                fechahora.get(Calendar.SECOND) );
        
        out.println("<br>");
        out.println("<font color=gray>");
        out.println("Del dia " + 
                fechahora.get(Calendar.DAY_OF_MONTH) + "/" + 
                (fechahora.get(Calendar.MONTH) + 1) + "/" + 
                fechahora.get(Calendar.YEAR) );
        
        out.println("</font>");
        out.println("</center>");
        out.println("</body>");
        out.println("</html>");
        out.close();
    }

    @Override
    public String getServletInfo() {
        return "Servlet Saludo";
    }

}
