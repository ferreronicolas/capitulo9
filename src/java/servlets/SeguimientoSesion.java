package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SeguimientoSesion extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        // obtener la sesion actual. Crear una si fuera necesario.
        HttpSession sesionCli = request.getSession(true);
        // Datos de la sesion
        String idSesion = sesionCli.getId();
        long fechaCreacion = sesionCli.getCreationTime();
        long fechaUltimoAcceso = sesionCli.getLastAccessedTime();

        // Incrementar el contador para esta pagina. El valor es
        // Guardado en la sesion del cliente con el nombre "cuenta.ss".
        Integer cuenta = (Integer) sesionCli.getAttribute("cuenta.ss");
        if (cuenta == null) {
            cuenta = new Integer(1);
        } else {
            cuenta = new Integer(cuenta.intValue() + 1);
        }
        sesionCli.setAttribute("cuenta.ss", cuenta);
        //sesionCli.setAttribute("atributo2", 3);
        Date fecha = new Date(fechaCreacion);
        
        out.println("<html>");
        out.println("<head><title>Seguimiento de Sesion</title></head>");
        out.println("<body>");
        out.println("<h1>Demostracion de seguimiento de una sesion</h1>");

        // Visualizar la cuenta para esta pagina
        out.println("Has visitado esta pagina " + cuenta
                + ((cuenta.intValue() == 1) ? " vez." : " veces."));
        out.println("<p>");
        out.println("<h2>Datos de tu sesion:</h2>");
        out.println("Sesion: " + idSesion);
        out.println("<br>Fue creada: "
                + fecha);
        out.println("<br>Fue accedida por ultima vez: "
                + (new Date(fechaUltimoAcceso)).toString());
        out.println("<br>Atributos:<br>");

        Enumeration nombresParams = sesionCli.getAttributeNames();
        while (nombresParams.hasMoreElements()) {
            String param = (String) nombresParams.nextElement();
            Object valor = sesionCli.getAttribute(param);
            out.println(param + ": " + valor.toString() + "<br>");
        }
        
        
        out.println("</body>");
        out.println("</html>");
        // Cerrar el flujo
        out.close();
    }

    @Override
    public String getServletInfo() {
        return "Servlet SeguimientoSesion";
    }

}
