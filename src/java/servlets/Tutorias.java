
package servlets;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Tutorias extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        try{
            // Abrir el fichero
            FileWriter fw = new FileWriter("/home/nico/tutorias.txt", true);
            PrintWriter fichTutorias = new PrintWriter(fw);
            
            // Tomar los datos recibidos del cliente
            Enumeration nombresParams = request.getParameterNames();
            while(nombresParams.hasMoreElements()){
                String param = (String) nombresParams.nextElement();
                String valor = request.getParameter(param);
                fichTutorias.println(param + ": " + valor);
            }
            fichTutorias.println("<FIN>");
            
            //Cerra el fichero
            fichTutorias.close();
            fw.close();
            
            // Responder al cliente
            out.println("<html>");
            out.println("<title>Concertar una tutoria</title>");
            out.println("Su peticion ha sisdo registrada.<br>Un saludo.<br>");
            out.println("</html>");
            
        }catch (IOException e){
            out.println("Hubo problemas cursando la solicitud. " + 
                    "<br>Por favor, intentelo otra vez.");
        }
        // Cerrar el flujo
        out.close();
        
    }

    @Override
    public String getServletInfo() {
        return "Servlet Turorias";
    }

}
